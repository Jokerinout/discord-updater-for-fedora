# Discord Updater

A simple Discord Updater for Fedora, made in Fedora 32.

To use this file ensure that you provide execute permission (ex: chmod +x) to the file.
Replace DOWNLOADDIRECTORY by the following one on your system.
Place the script in your $PATH directory, or directly where you have downloaded the discord archive.

## How to use it ?

- use this command

  `./discordUpdate`
